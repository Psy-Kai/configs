#!/bin/bash

set -e

stow -t ~ alacritty
stow -t ~ clang-format
stow -t ~ n-container-vim
stow -t ~ neovim
stow -t ~ oh-my-posh
stow -t ~ zsh
stow -t ~ zellij
