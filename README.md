# Psy-Kais Dotfiles and Configurations 💻

I am using NeoVim btw 😉

## My Setup

### Terminal

* [Alacritty](https://alacritty.org/)
* [bat](https://github.com/sharkdp/bat)
* [eza](https://github.com/eza-community/eza)
* [fzf](https://github.com/junegunn/fzf)
* [zsh](https://www.zsh.org/)
* [zoxide](https://github.com/ajeetdsouza/zoxide)

### Development

* [LazyGit](https://github.com/jesseduffield/lazygit)
* [n-container-vim](https://gitlab.com/Psy-Kai/n-container-vim)
* [NeoVim](https://neovim.io/)

#### NeoVim Submodule

To support [n-container-vim](https://gitlab.com/Psy-Kai/n-container-vim)
the __NeoVim__ configuration has to be in a separate repository.

### [Nerd Font](https://www.nerdfonts.com/)

`Hack Nerd Font` 👨‍💻  
Copy the files to `/usr/local/share/fonts/ttf/HackNerdFont`.

## Requirements

* [GNU Stow](https://www.gnu.org/software/stow/)

## Nice little Goodies

* [btop](https://github.com/aristocratos/btop)
