# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=100000
SAVEHIST=1000000
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/psy-kai/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

####################################################################################################
# User configuration
####################################################################################################

# https://unix.stackexchange.com/a/470959
zstyle ':completion:*' menu select

# https://github.com/zdharma-continuum/zinit?tab=readme-ov-file#manual
ZINIT_HOME="${XDG_DATA_HOME:-${HOME}/.local/share}/zinit/zinit.git"
[ ! -d $ZINIT_HOME ] && mkdir -p "$(dirname $ZINIT_HOME)"
[ ! -d $ZINIT_HOME/.git ] && git clone https://github.com/zdharma-continuum/zinit.git "$ZINIT_HOME"
source "${ZINIT_HOME}/zinit.zsh"

autoload -Uz _zinit
(( ${+_comps} )) && _comps[zinit]=_zinit

# update zinit
ZINIT_SELF_UPDATED="/tmp/${USER}_zinit_self_updated"
[ ! -f $ZINIT_SELF_UPDATED ] && zinit self-update && touch $ZINIT_SELF_UPDATED

# plugins
zinit ice wait lucid
zinit load zsh-users/zsh-autosuggestions
zinit load zdharma-continuum/fast-syntax-highlighting

# update plugins
ZINIT_PLUGINS_UPDATED="/tmp/${USER}_zinit_plugins_updated"
[ ! -f $ZINIT_PLUGINS_UPDATED ] && zinit update --parallel && touch $ZINIT_PLUGINS_UPDATED

# general environment setup
LOCAL_BIN="${HOME}/.local/bin"
mkdir -p $LOCAL_BIN
export LANG=en_US.UTF-8
export GOPATH="${HOME}/.go"
export PATH="${HOME}/.cargo/bin:${GOPATH}/bin:${LOCAL_BIN}:${HOME}/.local/go/bin:/opt/nvim-linux64/bin:${PATH}"
export EDITOR='nvim'

# https://github.com/sharkdp/bat?tab=readme-ov-file#man
export MANPAGER="sh -c 'col -bx | bat -l man -p'"
export MANROFFOPT="-c"

# use bat as a replacement for cat and less
alias cat='bat -pp'
alias grep='rg'
alias less='bat --paging=always'
alias ls='eza'

# https://github.com/junegunn/fzf?tab=readme-ov-file#setting-up-shell-integration
source <(fzf --zsh)
# https://github.com/ajeetdsouza/zoxide
eval "$(zoxide init --cmd cd zsh)"

# https://ohmyposh.dev
OH_MY_POSH_BIN="${LOCAL_BIN}/oh-my-posh"
[ ! -f $OH_MY_POSH_BIN ] && curl -s https://ohmyposh.dev/install.sh | bash -s -- -d $LOCAL_BIN
eval "$(oh-my-posh init zsh --config ~/.config/psy-kai.omp.json)"

# https://zellij.dev/
if [[ $TERM == "alacritty" && -z $ZELLIJ ]]; then
  zellij attach -c "psy-kai"
  exit
fi
