```sh
git config --global pull.rebase true
git config --global fetch.prune true
git config --global rebase.updateRefs true
git config --global diff.colorMoved dimmed-zebra
```
