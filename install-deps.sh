#!/bin/bash

set -e

[ ! $(command -v rustup) ] && curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
[ ! $(command -v bat) ] && cargo install --locked bat
[ ! $(command -v eza) ] && cargo install --locked eza
[ ! $(command -v rg) ] && cargo install --locked ripgrep
[ ! $(command -v zellij) ] && cargo install --locked zellij
[ ! $(command -v zoxide) ] && cargo install --locked zoxide

readonly GO_VERSION="1.23.0"
readonly GOPATH="${HOME}/.go"
[ ! $(command -v go) ] && curl -fL "https://go.dev/dl/go${GO_VERSION}.linux-amd64.tar.gz" | tar -xzf - -C ${HOME}/.local
[ ! $(command -v fzf) ] && go install github.com/junegunn/fzf@latest
[ ! $(command -v lazygit) ] && go install github.com/jesseduffield/lazygit@latest
